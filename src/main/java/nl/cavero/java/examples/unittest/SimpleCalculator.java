package nl.cavero.java.examples.unittest;

import static nl.cavero.java.examples.unittest.Operation.*;

public class SimpleCalculator implements Calculator<SimpleCalculator> {

	private int result;

	public SimpleCalculator(int firstInput) {
		this.result = firstInput;
	}
	
	@Override
	public SimpleCalculator plus(int input) {
		result = PLUS.execute(result, input);
		return this;
	}
	
	@Override
	public SimpleCalculator minus(int input) {
		result = MINUS.execute(result, input);
		return this;
	}
	
	@Override
	public SimpleCalculator times(int input) {
		result = TIMES.execute(result, input);
		return this;
	}
	
	@Override
	public SimpleCalculator dividedBy(int input) {
		result = DIVIDED_BY.execute(result, input);
		return this;
	}
	
	@Override
	public int calculate() {
		return result;
	}
	
}
