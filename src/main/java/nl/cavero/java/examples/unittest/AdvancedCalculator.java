package nl.cavero.java.examples.unittest;

import java.util.ArrayList;
import java.util.List;

public class AdvancedCalculator implements Calculator<AdvancedCalculator> {
	private final int firstInput;
	private final List<ComputationElement> elements;
	
	public AdvancedCalculator(int firstInput) {
		this.firstInput = firstInput;
		this.elements = new ArrayList<>();
	}

	@Override
	public AdvancedCalculator times(int input) {
		elements.add(new ComputationElement(Operation.TIMES, input));
		return this;
	}

	@Override
	public AdvancedCalculator plus(int input) {
		elements.add(new ComputationElement(Operation.PLUS, input));
		return this;
	}

	@Override
	public AdvancedCalculator minus(int input) {
		elements.add(new ComputationElement(Operation.MINUS, input));
		return this;
	}

	@Override
	public AdvancedCalculator dividedBy(int input) {
		elements.add(new ComputationElement(Operation.DIVIDED_BY, input));
		return this;
	}
	
	@Override
	public int calculate() {
		/*
		 * According to the rules of "Meneer Van Dale Wacht Op Antwoord", the order of executing the calculations in a complex calculation,
		 * should be V (multiply), D (divide), O (add) and A (subtract). Hence we need to compute the inputs as such.
		 * This immediately puts us into a difficult position, because we might need to modify the input to come to the correct conclusion: 
		 * we know the operation and the next input but the previous input might need to change because of previous computations.
		 */
		
		convertAllMinuses();
		
		//subdivide the list in sets of plusses
		List<List<ComputationElement>> subsets = new ArrayList<>();
		int previousIndex = 0;
		for (int i = 0; i < elements.size(); i++) {
			if (elements.get(i).operation == Operation.PLUS) {
				System.err.println("" + elements.get(i));
				subsets.add(elements.subList(previousIndex, i));
				previousIndex = i;
			}
		}
		subsets.add(elements.subList(previousIndex, elements.size()));
		
		
		System.err.println(subsets);
		
		int total = firstInput;
		for (List<ComputationElement> subset : subsets) {
			total += computeTotal(subset);
		}
		
		return total;
	}
	
	private void convertAllMinuses() {
		for (ComputationElement ce : elements) {
			if (ce.operation == Operation.MINUS) {
				ce.operation = Operation.PLUS;
				ce.secondValue = -ce.secondValue;
			}
		}
	}
	
	private int computeTotal(List<ComputationElement> subset) {
		if (subset.isEmpty()) {
			return 0;
		}
		
		int total = subset.get(0).secondValue;
		if (subset.size() == 1) {
			return total;
		}
		
		for (int i = 1; i < subset.size(); i++) {
			ComputationElement ce = subset.get(i);
			if (ce.operation == Operation.TIMES) {
				total = ce.compute(total);
			}
		}
		for (int i = 1; i < subset.size(); i++) {
			ComputationElement ce = subset.get(i);
			if (ce.operation == Operation.DIVIDED_BY) {
				total = ce.compute(total);
			}
		}
		
		return total;
	}
	
	private static class ComputationElement {
		private Operation operation;
		private int secondValue;
		
		public ComputationElement(Operation operation, int secondValue) {
			this.operation = operation;
			this.secondValue = secondValue;
		}
		
		public int compute(int firstValue) {
			return operation.execute(firstValue, secondValue);
		}
		
		@Override
		public String toString() {
			return "[" + operation + ", " + secondValue + "]";
		}
	}
	
}
