package nl.cavero.java.examples.unittest;

public enum Operation {
	PLUS {
		@Override
		public int execute(int first, int second) {
			return first + second;
		}
	},
	MINUS {
		@Override
		public int execute(int first, int second) {
			return first - second;
		}
	},
	TIMES {
		@Override
		public int execute(int first, int second) {
			return first * second;
		}
	},
	DIVIDED_BY {
		@Override
		public int execute(int first, int second) {
			return first / second;
		}
	},
	;
	
	public abstract int execute(int first, int second);
}
