package nl.cavero.java.examples.unittest;

public interface Calculator<T> {

	T times(int input);
	T plus(int input);
	T minus(int input);
	T dividedBy(int input);
	int calculate();
	
}
