package nl.cavero.java.examples.unittest;

import static org.junit.Assert.assertEquals;
import nl.cavero.java.examples.unittest.Calculator;
import nl.cavero.java.examples.unittest.SimpleCalculator;

import org.junit.Before;
import org.junit.Test;

public class SimpleCalculatorTest {

	private Calculator<SimpleCalculator> calculator;
	
	@Before //Gets executed before each test case. See javadoc and junit documention for more details
	public void setup() {
		calculator = new SimpleCalculator(6);
	}
	
	@Test
	public void plus_adds_the_given_number_to_the_total() {
		assertEquals(10, calculator.plus(4).calculate());
	}
	
	@Test
	public void times_multiplies_the_existing_total_and_the_given_number() {
		assertEquals(24, calculator.times(4).calculate());
	}
	
	@Test
	public void minus_subtracts_the_given_number_from_the_total() {
		assertEquals(2, calculator.minus(4).calculate());
	}
	
	@Test
	public void dividedBy_divides_the_existing_total_by_the_given_number() {
		assertEquals(3, calculator.dividedBy(2).calculate());
	}
	
	@Test
	public void the_simple_calculator_computes_intermediate_results_and_is_thus_inherently_flawed() {
		/*
		 * Actually, this test is quite bad, because it tests against an implementation detail: the simple calculator uses intermediate results.
		 * To improve, write the desired functionality and the expected outcome. Don't validate implementation details, because it makes the tests break
		 * when you change the implementation.
		 * 
		 * To demonstrate this, replace the SimpleCalculator with the AdvancedCalculator. This will break this test, because the original test, as given below,
		 * is written with the implementation in mind and not with the desired outcome. 
		 */
		
		assertEquals(10, calculator.dividedBy(2).times(4).plus(14).minus(6).times(4).dividedBy(8).calculate());
	}
	
}
