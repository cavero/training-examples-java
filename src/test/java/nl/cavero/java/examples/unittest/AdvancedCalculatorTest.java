package nl.cavero.java.examples.unittest;

import static org.junit.Assert.*;
import nl.cavero.java.examples.unittest.AdvancedCalculator;

import org.junit.Test;

public class AdvancedCalculatorTest {

	@Test
	public void test() {
		int total = new AdvancedCalculator(6).minus(3).times(7).plus(2).dividedBy(4).times(9).calculate();
		assertEquals(-11, total);
	}
	
	@Test
	public void test2() {
		int total = new AdvancedCalculator(6).dividedBy(2).times(4).plus(14).minus(6).times(4).dividedBy(8).calculate();
		assertEquals(23, total);
	}
	
}
