package nl.cavero.java.examples.unittest;

import static org.junit.Assert.*;
import static nl.cavero.java.examples.unittest.Operation.*;

import org.junit.Test;

public class OperationTest {

	@Test
	public void using_plus_adds_the_two_numbers() {
		assertEquals(5, PLUS.execute(2, 3));
	}
	
	@Test
	public void the_order_doesnt_matter_when_adding() {
		assertEquals(PLUS.execute(2, 3), PLUS.execute(3, 2));
	}
	
	@Test
	public void adding_negative_numbers_results_in_the_negative_number_being_subtracted_from_the_positive_number() {
		assertEquals(4, PLUS.execute(5, -1));
	}
	
	@Test
	public void adding_zero_returns_the_first_value_unchanged() {
		assertEquals(11, PLUS.execute(0, 11));
	}
	
	@Test
	public void plus_with_int_max_and_a_positive_value_wraps_around() {
		assertEquals(Integer.MIN_VALUE, PLUS.execute(Integer.MAX_VALUE, 1));
	}
	
	@Test
	public void plus_with_int_min_and_a_negative_value_wraps_around() {
		assertEquals(Integer.MAX_VALUE, PLUS.execute(Integer.MIN_VALUE, -1));
	}

	@Test
	public void using_minus_subtracts_the_second_number_from_the_first() {
		assertEquals(5, MINUS.execute(8, 3));
	}
	
	@Test
	public void the_order_matters_when_subtracting() {
		assertNotEquals(MINUS.execute(3, 8), MINUS.execute(8, 3));
	}
	
	@Test
	public void subtracting_negative_numbers_results_in_adding_the_abs_value_of_the_second_number_to_the_first() {
		assertEquals(11, MINUS.execute(8, -3));
	}
	
	@Test
	public void subtracting_zero_returns_the_first_value_unchanged() {
		assertEquals(8, MINUS.execute(8, 0));
	}
	
	@Test
	public void minus_with_int_max_and_a_negative_value_wraps_around() {
		assertEquals(Integer.MIN_VALUE, MINUS.execute(Integer.MAX_VALUE, -1));
	}
	
	@Test
	public void minus_with_int_min_and_a_positive_value_wraps_around() {
		assertEquals(Integer.MAX_VALUE, MINUS.execute(Integer.MIN_VALUE, 1));
	}
	
	@Test
	public void using_times_will_multiply_the_numbers() {
		assertEquals(24, TIMES.execute(4, 6));
	}
	
	@Test
	public void the_order_doesnt_matter_when_multiplying() {
		assertEquals(TIMES.execute(6, 4), TIMES.execute(4, 6));
	}
	
	@Test
	public void multiplying_with_one_negative_number_makes_the_total_negative() {
		assertEquals(-24, TIMES.execute(-4, 6));
	}
	
	@Test
	public void multiplying_with_two_negative_numbers_makes_the_total_positive() {
		assertEquals(24, TIMES.execute(-4, -6));
	}
	
	@Test
	public void multiplying_zero_results_in_the_total_being_zero() {
		assertEquals(0, TIMES.execute(4, 0));
	}
	
	@Test
	public void multiplying_large_numbers_will_cause_a_wrap_around() {
		assertEquals(-2, TIMES.execute(Integer.MAX_VALUE, 2));
	}
	
	@Test
	public void using_divided_by_will_divide_the_first_value_by_the_second_value() {
		assertEquals(12, DIVIDED_BY.execute(36, 3));
	}
	
	@Test
	public void the_order_matters_when_dividing() {
		assertNotEquals(DIVIDED_BY.execute(3, 36), DIVIDED_BY.execute(36, 3));
	}
	
	@Test
	public void using_divided_by_will_use_integer_division() {
		assertEquals(5, DIVIDED_BY.execute(26, 5));
	}
	
	@Test
	public void dividing_with_either_number_negative_gives_a_negative_result() {
		assertEquals(-5, DIVIDED_BY.execute(26, -5));
		assertEquals(-5, DIVIDED_BY.execute(-26, 5)); //Actually, this is not very nice, because ideally, you're supposed to have only one assert per test.
	}
	
	@Test
	public void dividing_zero_always_results_in_zero() {
		assertEquals(0, DIVIDED_BY.execute(0, 10000000));
	}
	
	@Test(expected = ArithmeticException.class) //Notice the expected exception here and the missing 'assert' in the method body. 
	public void dividing_by_zero_always_results_in_an_exception() {
		DIVIDED_BY.execute(10000000, 0);
	}
	
}
